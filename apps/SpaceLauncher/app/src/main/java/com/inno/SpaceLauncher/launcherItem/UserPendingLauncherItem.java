package com.inno.SpaceLauncher.launcherItem;

import android.content.Context;

import com.inno.SpaceLauncher.R;

/**
 * 설치 대기 중인 사용자 APP
 */
public class UserPendingLauncherItem extends UserLauncherItem {
    private String mTitle;


    public UserPendingLauncherItem(Context context, String packageName, String title) {
        super(context, packageName);
        mTitle = title;
    }

    @Override
    public String getTitle() {
        return mTitle == null ? "Unknown" : mTitle;
    }

    @Override
    public int getImageId() {
        return R.drawable.app_icon_holder;
    }

    @Override
    public void onClick() {
    }

    @Override
    public boolean onLongClick(Context context) {
        return false;
    }
}
