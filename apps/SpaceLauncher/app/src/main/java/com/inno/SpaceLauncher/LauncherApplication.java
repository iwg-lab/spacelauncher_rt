package com.inno.SpaceLauncher;

import android.app.Application;

import com.inno.SpaceLauncher.util.pref.PrefUtils;

public class LauncherApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        PrefUtils.createInstance(this);
    }
}
