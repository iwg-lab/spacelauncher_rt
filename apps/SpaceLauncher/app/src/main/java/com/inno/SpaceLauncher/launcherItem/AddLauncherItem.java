package com.inno.SpaceLauncher.launcherItem;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

import com.inno.SpaceLauncher.R;

import com.inno.SpaceLauncher.install.UserAppActivity;

public class AddLauncherItem implements LauncherItem {

    private Context mContext;

    public AddLauncherItem(Context context){
        mContext = context;
    }

    @Override
    public int getImageId() {
        return R.drawable.main_app_myapk;
    }

    @Override
    public Drawable getImageDrawable() {
        return null;
    }

    @Override
    public int getTitleId() {
        return R.string.main_app_myapk;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getPackageName() {
        return null;
    }

    @Override
    public void onClick() {
        Intent install = new Intent(mContext, UserAppActivity.class);
        install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        install.putExtra("position",0);
        mContext.startActivity(install);

    }

    @Override
    public boolean onLongClick(Context context) {
        return false;
    }

    @Override
    public boolean needsWifiCheck() {
        return false;
    }

    @Override
    public boolean needsDeviceRegistrationCheck() {
        return false;
    }
}
