package com.inno.SpaceLauncher.install;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import com.inno.SpaceLauncher.config.AppList;
import com.inno.SpaceLauncher.launcherItem.UserLauncherItem;
import com.inno.SpaceLauncher.launcherItem.manager.LauncherItemManager;

import java.util.List;

public class PackageManagerReceiver extends BroadcastReceiver {

    private static final String TAG = "PackageManagerReceiver";


    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Uri data = intent.getData();

//        Log.d(TAG, "PackageManagerReceiver > " + action);
//        Log.d(TAG, "PackageManagerReceiver data> " + data);

        if (data == null) {
            return;
        }
        String packageName = data.getSchemeSpecificPart();
        if (Intent.ACTION_PACKAGE_ADDED.equals(action)) {
            // System 앱은 앱 등록하지 않음
            if (!isSystemApp(context, packageName) && !AppList.isDefaultAppPackage(packageName)) {
                LauncherItemManager.getInstance(context).addUserItem(new UserLauncherItem(context, packageName));
                LauncherItemManager.getInstance(context).callOnItemChanged();
            }
        } else if (Intent.ACTION_PACKAGE_REMOVED.equals(action)) {
            // 등록 된 앱 삭제
            LauncherItemManager.getInstance(context).removeUserItem(new UserLauncherItem(context, packageName));
            LauncherItemManager.getInstance(context).callOnItemChanged();
        }
    }

    public static boolean isSystemApp(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        List<PackageInfo> list = pm.getInstalledPackages(0);

        for (PackageInfo pi : list) {
            ApplicationInfo ai = null;
            try {
                ai = pm.getApplicationInfo(pi.packageName, 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if ((ai.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                if (pi.packageName.equals(packageName)) {
                    Log.d(TAG, packageName + " is system package");
                    return true;
                }
            }
        }
        return false;
    }

}
