package com.inno.SpaceLauncher.install;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.content.FileProvider;

import com.inno.SpaceLauncher.R;
import com.inno.SpaceLauncher.launcherItem.UserLauncherItem;
import com.inno.SpaceLauncher.launcherItem.UserPendingLauncherItem;
import com.inno.SpaceLauncher.launcherItem.manager.LauncherItemManager;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SelectApkActivity extends Activity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    // NOTE: 아이콘 표시 사용시 설치 오류시에 대한 처리가 힘듬
    // - 일단 설치 중 아이콘 표시하지 않음
    private static final boolean SHOW_INSTALLING_ICON = false;

    private static final String INTERNAL_SD = Environment.getExternalStorageDirectory().getPath();
    private static final String EXTERNAL_SD = "/mnt/external_sd/";
    private static final String SEND_ANYWHERE_DIR = "SendAnywhere";

    private ListView mListView;
    private List<File> mFileList = new ArrayList<File>();
    private LayoutInflater mInflater;
    private int mPosition; //설치 선택된 위치

    private AlertDialog mAlertDialog;
    private storageMountReceiver mStorageReceiver;
    private PackageManager mPackageManager;

    private int mLastVisiblePosition;
    private int mScrollY;

    private static final String[] sFoldersToList = new String[]{
            INTERNAL_SD,
            INTERNAL_SD + "/Download",
            INTERNAL_SD + "/" + SEND_ANYWHERE_DIR,
            EXTERNAL_SD,
    };
    private List<SDFileObserver> mFileObservers = new ArrayList<SDFileObserver>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPackageManager = getPackageManager();
        mPosition = getIntent().getIntExtra("position", -1);
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mStorageReceiver = new storageMountReceiver();
        setContentView(R.layout.activity_select_apk);

        mListView = (ListView) findViewById(R.id.apk_list);

        TextView emptyView = (TextView) findViewById(R.id.empty_view);
        emptyView.setText(Html.fromHtml(getString(R.string.apk_install_help)));
        mListView.setEmptyView(emptyView);

//		mListView.setAdapter(new fileListAdapter());
        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);

        TextView description = (TextView) findViewById(R.id.description);
        description.setText(R.string.selection_apk_description);
        //String txt = getString(R.string.selection_apk_description);
        //description.setText(Html.fromHtml(txt));

        ImageView backButton = (ImageView) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageView managementButton = (ImageView) findViewById(R.id.management_app);
        managementButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PackageManagement.class));
            }
        });
        for (String folder : sFoldersToList) {
            mFileObservers.add(new SDFileObserver(folder));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshFile();
        mListView.setSelectionFromTop(mLastVisiblePosition, mScrollY);
        for (SDFileObserver sfo : mFileObservers) {
            sfo.startWatching();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_EJECT);
        filter.addDataScheme("file");
        registerReceiver(mStorageReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        getListScrollInfo();

        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
        for (SDFileObserver sfo : mFileObservers) {
            sfo.stopWatching();
        }
        unregisterReceiver(mStorageReceiver);
    }

    @Override
    public void onItemClick(final AdapterView<?> adapterView, View view, int position, long l) {
        ListView listView = (ListView) adapterView;
        final File file = (File) listView.getItemAtPosition(position);
        final String apkPath = file.getPath();
        final PackageInfo packageInfo = mPackageManager.getPackageArchiveInfo(apkPath,
                PackageManager.GET_ACTIVITIES);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (packageInfo != null && isInstalledApk(packageInfo) == 0) {
            // 새로운 앱 설치
            installApk(apkPath);
            if (SHOW_INSTALLING_ICON) {
                packageInfo.applicationInfo.sourceDir = apkPath;
                packageInfo.applicationInfo.publicSourceDir = apkPath;
                String appName = (String) packageInfo.applicationInfo.loadLabel(mPackageManager);
                LauncherItemManager.getInstance(this).addUserItem(
                        new UserPendingLauncherItem(SelectApkActivity.this, packageInfo.packageName, appName));
            }
        } else if (packageInfo != null && isInstalledApk(packageInfo) == 1) {
            // 이미 설치 되어 있음
            boolean result = LauncherItemManager.getInstance(this).isAppInstalled(packageInfo.packageName);

            if (result) {
                // 이미 등록되어 있은 앱
                mAlertDialog = builder
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                LauncherItemManager.getInstance(getApplicationContext()).moveUserItemToEnd(
                                        new UserLauncherItem(SelectApkActivity.this,
                                                packageInfo.packageName));
                                finish();
                            }
                        })
                        .setMessage(getString(R.string.launcher_item_installed) + "\r\n" + getString(R.string.launcher_item_move))
                        .create();
            } else {
                // 설치는 되어 있지만 등록 되어 있지 않은 앱
                mAlertDialog = builder
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                LauncherItemManager.getInstance(getApplicationContext()).addUserItem(
                                        new UserLauncherItem(SelectApkActivity.this,
                                                packageInfo.packageName));
                                finish();
                            }
                        })
                        .setMessage(getString(R.string.launcher_item_installed) + "\r\n" + getString(R.string.launcher_item_add))
                        .create();
            }
            mAlertDialog.show();
        } else if (packageInfo != null && isInstalledApk(packageInfo) == -1) {
            PackageInfo installedInfo = getInstalledPackageInfo(packageInfo.packageName);
            LinearLayout msgLayout = (LinearLayout) mInflater.inflate(R.layout.file_version_info, null);

            ((TextView) msgLayout.findViewById(R.id.msg)).setText(R.string.launcher_item_installed_upper);
            ((TextView) msgLayout.findViewById(R.id.installed_info)).setText(installedInfo.versionName + " (" + installedInfo.versionCode + ")");
            ((TextView) msgLayout.findViewById(R.id.file_info)).setText(packageInfo.versionName + " (" + packageInfo.versionCode + ")");

            // 설치된 버전이 높은 경우
            mAlertDialog = builder
                    .setNegativeButton(android.R.string.ok, null)
                    .setView(msgLayout)
                    .create();
            mAlertDialog.show();

        } else {
            // 설치 불가 앱
            mAlertDialog = builder
                    .setNegativeButton(android.R.string.ok, null)
                    .setMessage(R.string.launcher_item_install_error).create();
            mAlertDialog.show();
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
        ListView listView = (ListView) adapterView;
        final File file = (File) listView.getItemAtPosition(position);
        String path = file.getPath();

        PackageInfo packageInfo = getPackageInfoFromApk(path);
        String versionInfo = null;
        String name = null;
        Drawable icon = null;
        if (packageInfo != null) {
            name = getAppNameFromApk(path);
            icon = getAppIconFromApk(path);
            versionInfo = String.format("Version : %s\r\nVersionName : %s\r\n%s", packageInfo.versionCode, packageInfo.versionName, getInstallable(file));
        } else {
            name = getString(R.string.unknown);
            icon = getAppIconFromApk(path);
            versionInfo = String.format("Version : %s\r\nVersionName : %s\r\n%s", getString(R.string.unknown), getString(R.string.unknown), getInstallable(file));
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        mAlertDialog = builder.setPositiveButton(android.R.string.ok, null)
                .setTitle(name)
                .setIcon(icon)
                .setMessage(versionInfo)
                .create();
        mAlertDialog.show();
        return true;
    }

    /**
     * 리스트 스크롤 위치 기억을 위해
     */
    private void getListScrollInfo() {
        mLastVisiblePosition = mListView.getFirstVisiblePosition();
        View v = mListView.getChildAt(0);
        if (v != null)
            mScrollY = v.getTop();
        else
            mScrollY = 0;
    }

    /**
     * 앱 설치 여부
     *
     * @param packageInfo
     * @return
     */
    private int isInstalledApk(PackageInfo packageInfo) {
        mPackageManager = getPackageManager();
        List<PackageInfo> packages = mPackageManager.getInstalledPackages(PackageManager.GET_META_DATA);
        for (PackageInfo pi : packages) {
            if (packageInfo.packageName.equals(pi.packageName)) {
                if (packageInfo.versionCode == pi.versionCode) {
                    return 1;
                } else if (packageInfo.versionCode < pi.versionCode) {
                    return -1;
                }
            }
        }
        return 0;
    }

    /**
     * 설치된 apk의 packageinfo
     *
     * @param packageName
     * @return
     */
    private PackageInfo getInstalledPackageInfo(String packageName) {
        mPackageManager = getPackageManager();
        List<PackageInfo> packages = mPackageManager.getInstalledPackages(PackageManager.GET_META_DATA);
        for (PackageInfo pi : packages) {
            if (pi.packageName.equals(packageName)) {
                return pi;
            }
        }
        return null;
    }

    /**
     * 앱 설치
     *
     * @param apkPath 설치할 파일 경로
     */
    private void installApk(String apkPath) {
        File apkFile = new File(apkPath);
        Uri apkUri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".files", apkFile);
        Intent installApk = new Intent(Intent.ACTION_VIEW);
        installApk.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        installApk.setDataAndType(apkUri, "application/vnd.android.package-archive");
        installApk.putExtra(Intent.EXTRA_RETURN_RESULT, true);

        startActivityForResult(installApk, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("TEST", "SelectApk onActivityResult() requestCode >" + requestCode + " resultCode >" + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                // 정상 설치 완료
                finish();
            } else if (resultCode == RESULT_FIRST_USER) {
                // 설치 오류
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                if (data != null) {
                    final String EXTRA_INSTALL_RESULT = "android.intent.extra.INSTALL_RESULT";
                    int installResult = data.getIntExtra(EXTRA_INSTALL_RESULT, 0);
//                    int installResult = data.getIntExtra(Intent.EXTRA_INSTALL_RESULT, 0);

                    final int INSTALL_FAILED_ALREADY_EXISTS = -1;
                    final int INSTALL_FAILED_INSUFFICIENT_STORAGE = -4;

                    switch (installResult) {
//                        case PackageManager.INSTALL_FAILED_ALREADY_EXISTS:
                        case INSTALL_FAILED_ALREADY_EXISTS:
                            builder.setMessage(R.string.launcher_item_installed);
                            break;
//                        case PackageManager.INSTALL_FAILED_INSUFFICIENT_STORAGE :
                        case INSTALL_FAILED_INSUFFICIENT_STORAGE :
                            builder.setMessage(R.string.launcher_item_install_freespace_error);
                            break;
                        default :
                            builder.setMessage(R.string.launcher_item_install_fail);
                            break;
                    }
                } else {
                    builder.setMessage(R.string.launcher_item_install_fail);
                }

                mAlertDialog = builder
                        .setNegativeButton(R.string.confirm, null)
                        .create();

                mAlertDialog.show();

                if (SHOW_INSTALLING_ICON) {
                    LauncherItemManager.getInstance(getApplicationContext()).removeLastUserItem();
                }
            } else if (resultCode == RESULT_CANCELED) {
                // 설치 취소
                if (SHOW_INSTALLING_ICON) {
                    LauncherItemManager.getInstance(getApplicationContext()).removeLastUserItem();
                }
            }
        }
    }

    class fileListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mFileList.size();
        }

        @Override
        public Object getItem(int i) {
            return mFileList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            ViewHolder holder;
            if (view == null) {
                view = mInflater.inflate(R.layout.list_item_select_apk, null);

                holder = new ViewHolder();
                holder.appIcon = (ImageView) view.findViewById(R.id.app_icon);
                holder.appName = (TextView) view.findViewById(R.id.app_name);
                holder.fileName = (TextView) view.findViewById(R.id.file_name);
                holder.locationSd = (TextView) view.findViewById(R.id.sd_location);
                /*holder.installable = (TextView) view.findViewById(R.id.installable);*/

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            File file = mFileList.get(position);
            String appName = getAppNameFromApk(file.getPath());
            Drawable appIcon = getAppIconFromApk(file.getPath());

            holder.appIcon.setBackgroundDrawable(appIcon);
            holder.appName.setText(String.format("%s ", appName != null ? appName : getString(R.string.unknown)));
            holder.fileName.setText(String.format("%s", file.getName()));
            //holder.locationSd.setText(file.getParent().startsWith(INTERNAL_SD) ?
            //        R.string.internal_sd : R.string.external_sd);
            /*holder.installable.setText(String.format("%s", getInstallable(file)));*/
            return view;
        }

        class ViewHolder {
            ImageView appIcon;
            TextView appName;
            TextView fileName;
            TextView locationSd;
            /*public TextView installable;*/
        }
    }

    private String getInstallable(File file) {
        PackageInfo packageInfo = mPackageManager.getPackageArchiveInfo(file.getPath(), 0); //설치할 파일 정보

        if (packageInfo == null) {
            return getString(R.string.impossible_install);
        }

        List<PackageInfo> packages = mPackageManager.getInstalledPackages(PackageManager.GET_META_DATA);
        for (PackageInfo pi : packages) {
            if (packageInfo.packageName.equals(pi.packageName)) {
                if (packageInfo.versionCode == pi.versionCode) {
                    return getString(R.string.installed);
                } else if (packageInfo.versionCode < pi.versionCode) {
                    return getString(R.string.upper_installed);
                }
            }
        }
        return getString(R.string.possible_install);

    }

    /**
     * App 이름 가져오기
     *
     * @param path
     * @return
     */
    public String getAppNameFromApk(final String path) {
        PackageInfo pi = mPackageManager.getPackageArchiveInfo(path, 0);

        if (pi != null) {
            pi.applicationInfo.sourceDir = path;
            pi.applicationInfo.publicSourceDir = path;
            return (String) pi.applicationInfo.loadLabel(mPackageManager);
        } else {
            return null;
        }
    }

    /**
     * App 아이콘 가져오기
     *
     * @param path
     * @return
     */
    public Drawable getAppIconFromApk(final String path) {
        PackageInfo pi = mPackageManager.getPackageArchiveInfo(path, 0);

        if (pi != null) {
            pi.applicationInfo.sourceDir = path;
            pi.applicationInfo.publicSourceDir = path;
            return pi.applicationInfo.loadIcon(mPackageManager);
        } else {
            return getResources().getDrawable(R.drawable.ic_launcher);
        }
    }

    /**
     * apk파일에서 packageinfo 가져오기
     *
     * @param path
     * @return
     */
    private PackageInfo getPackageInfoFromApk(final String path) {
        return mPackageManager.getPackageArchiveInfo(path, 0);
    }

    /**
     * apk 파일 리스트 생성
     *
     * @param path 리스트를 생성할 경로
     */
    private void getFileList(final String path) {
        if (TextUtils.isEmpty(path)) return;

        File mCurrentPath = new File(path);
        //Log.d("SelectApkActivity", "getFileList " + mCurrentPath.getName());
        if (mCurrentPath.isDirectory()) {
            //Log.d("SelectApkActivity", "isDirectory()");
            // mFileList.clear();
            File[] files = mCurrentPath.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    if (pathname.isDirectory()) {
                        return false;
                    } else {
                        String filePath = pathname.getPath();
                        Log.d("SelectApkActivity", "filePath " + filePath);
                        return (filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length()))
                                .compareToIgnoreCase("apk") == 0;
                    }
                }
            });
            if (files != null) {
                Collections.addAll(mFileList, files);
            }
            Collections.sort(mFileList, new Comparator<File>() {
                @Override
                public int compare(File lhs, File rhs) {
                    if (lhs.isDirectory() && rhs.isFile()) {
                        return -1;
                    } else if (lhs.isFile() && rhs.isDirectory()) {
                        return 1;
                    } else {
                        return lhs.getPath().compareToIgnoreCase(rhs.getPath());
                    }
                }
            });
        }
    }

    protected void refreshFile() {
        mFileList.clear();
        for (String folder : sFoldersToList) {
            getFileList(folder);
        }
        mListView.setAdapter(new fileListAdapter());
    }

    public class storageMountReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_MEDIA_MOUNTED) || action.equals(Intent.ACTION_MEDIA_EJECT)) {
                refreshFile();
            }
        }
    }

    private class SDFileObserver extends FileObserver {

        SDFileObserver(String path) {
            super(path);
        }

        @Override
        public void onEvent(int i, String path) {
            if (i == FileObserver.CLOSE_WRITE || i == FileObserver.DELETE) {
                if (path.toLowerCase().endsWith(".apk")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            refreshFile();
                        }
                    });
                }
            }
        }
    }
}