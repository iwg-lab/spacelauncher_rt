package com.inno.SpaceLauncher.launcherItem;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.inno.SpaceLauncher.launcherItem.LauncherItem;

public class BuiltInLauncherItem implements LauncherItem {

    private Context mContext;
    private int mImageResId;
    private int mTitleResId;
    private String mPackageName;
    private String mActivityName;
    private String mExtraName;
    private String mExtraValue;
    private boolean mNeedsWifiCheck;
    private boolean mNeedsDeviceRegistrationCheck;

    public BuiltInLauncherItem(Context context, int imageResId, int titleResId, String packageName, String activityName,
                               String extraName, String extraValue, boolean needsWifiCheck, boolean needsDeviceRegistrationCheck) {
        mContext = context;
        mImageResId = imageResId;
        mTitleResId = titleResId;
        mPackageName = packageName;
        mActivityName = activityName;
        mExtraName = extraName;
        mExtraValue = extraValue;
        mNeedsWifiCheck = needsWifiCheck;
        mNeedsDeviceRegistrationCheck = needsDeviceRegistrationCheck;
    }

    public BuiltInLauncherItem(Context context, int imageResId, int titleResId, String packageName, String activityName,
                               boolean needsWifiCheck, boolean needsDeviceRegistrationCheck) {
        this(context, imageResId, titleResId, packageName, activityName, null, null, needsWifiCheck, needsDeviceRegistrationCheck);

    }

    public BuiltInLauncherItem(Context context, int imageResId, int titleResId, String packageName,
                               boolean needsWifiCheck, boolean needsDeviceRegistrationCheck) {
        this(context, imageResId, titleResId, packageName, null, null, null, needsWifiCheck, needsDeviceRegistrationCheck);
    }

    public BuiltInLauncherItem(Context context, int imageResId, int titleResId, String packageName) {
        this(context, imageResId, titleResId, packageName, null, null, null, false, false);
    }

    @Override
    public int getImageId() {
        return mImageResId;
    }

    @Override
    public Drawable getImageDrawable() {
        return null;
    }

    @Override
    public int getTitleId() {
        return mTitleResId;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getPackageName() {
        return null;
    }

    @Override
    public void onClick() {

    }

    @Override
    public boolean onLongClick(Context context) {
        return false;
    }

    @Override
    public boolean needsWifiCheck() {
        return false;
    }

    @Override
    public boolean needsDeviceRegistrationCheck() {
        return false;
    }
}
