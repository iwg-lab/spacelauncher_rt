package com.inno.SpaceLauncher.install.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.inno.SpaceLauncher.R;
import com.inno.SpaceLauncher.config.AppList;
import com.inno.SpaceLauncher.install.AppPackageItem;
import com.inno.SpaceLauncher.install.PageUtils;
import com.inno.SpaceLauncher.install.adapter.ListViewAdapter;
import com.inno.SpaceLauncher.launcherItem.UserLauncherItem;
import com.inno.SpaceLauncher.launcherItem.manager.LauncherItemManager;

import java.util.ArrayList;
import java.util.List;

public class AppManagementFragment extends Fragment {
    private static final String TAG = "ManagementFragment";

    private PackageManager mPackageManager;
    private ListView mListView;
    private ListViewAdapter mAdapter;
    private Context mContext;
    List<PackageInfo> filteredPackages;
    List<PackageInfo> packages;

    TextView tvPackage_empty, tv_page;
    ImageView mPrevBtn, mNextBtn;
    LinearLayout pagination_layout;

    private int mCurrentPage = 1;
    private int mTotalPage = 0;
    private int mShowPerPage = 7;
    private PageUtils pageUtils;

    private final ContentObserver mItemChangeObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            refreshViews();
        }
    };

    private void refreshViews() {
        // reload items
        getInstalledPackages();
        mAdapter.notifyDataSetChanged();

        int totalRow = filteredPackages.size();
        mTotalPage = pageUtils.getTotalPage(totalRow,mShowPerPage);
        setPageView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (mContext != null) {
            mContext = null;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_management, container, false);

        TextView description = (TextView) view.findViewById(R.id.description);
        description.setText(R.string.app_management_description);

        tvPackage_empty = (TextView) view.findViewById(R.id.tv_package_empty);
        tvPackage_empty.setText(Html.fromHtml(getString(R.string.package_empty)));
        pagination_layout = (LinearLayout)view.findViewById(R.id.pagination_layout);
        pageUtils = new PageUtils();

        mPackageManager = mContext.getPackageManager();

        tv_page = (TextView)view.findViewById(R.id.text_page);
        mAdapter = new ListViewAdapter(mContext);

        int index = pageUtils.getIndexRowStart(mShowPerPage, mCurrentPage);

        mAdapter.setOnListItemClickListener(new ListViewAdapter.OnListItemClickListener() {
            @Override
            public void onItemClick(AppPackageItem item) {
                String pkg = item.getPackageName();
                if (AppList.isDefaultAppPackage(pkg)) {
                    packageCheckDialogForDefaultApp(pkg);
                } else {
                    packageCheckDialog(pkg);
                }
            }
        });

        mListView = (ListView) view.findViewById(R.id.lv_package_list);
        mAdapter.setIndex(index);
        mListView.setAdapter(mAdapter);
        mListView.setEmptyView(tvPackage_empty);

        getInstalledPackages();
        int totalRow = filteredPackages.size();
        mTotalPage = pageUtils.getTotalPage(totalRow,mShowPerPage);

        tv_page.setText(getString(R.string.tv_page, mCurrentPage, mTotalPage));

        mPrevBtn = (ImageView) view.findViewById(R.id.prevBtn);
        mPrevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCurrentPage > 1) {
                    try{
                        mCurrentPage--;
                        int index = pageUtils.getIndexRowStart(mShowPerPage, mCurrentPage);
                        mAdapter.setIndex(index);
                        mListView.setAdapter(mAdapter);
                        tv_page.setText(getString(R.string.tv_page, mCurrentPage, mTotalPage));
                        mPrevBtn.setEnabled(true);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });

        mNextBtn = (ImageView)view.findViewById(R.id.nextBtn);
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCurrentPage < mTotalPage){
                    try {
                        mCurrentPage++;
                        int index = pageUtils.getIndexRowStart(mShowPerPage, mCurrentPage);
                        mAdapter.setIndex(index);
                        mListView.setAdapter(mAdapter);
                        tv_page.setText(getString(R.string.tv_page, mCurrentPage, mTotalPage));
                        mNextBtn.setEnabled(true);
                    }catch (Exception e){
                        e.printStackTrace();

                    }
                }
            }
        });
        setPageView();
        return view;
    }

    @Override
    public void onResume() {
        refreshViews();
        super.onResume();
    }

    public void setPackageEmptyView(boolean visible) {
        if (tvPackage_empty == null) {
            Log.d(TAG, "tvPackage_empty is null");
            return;
        }

        if (visible) {
            tvPackage_empty.setText(R.string.package_empty);
            tvPackage_empty.setVisibility(View.VISIBLE);
            pagination_layout.setVisibility(View.GONE);
            mListView.setVisibility(View.GONE);
        } else {
            pagination_layout.setVisibility(View.VISIBLE);
            tvPackage_empty.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);
        }
    }

    public void getInstalledPackages() {
        packages = mPackageManager.getInstalledPackages(PackageManager.GET_META_DATA);
        filteredPackages = new ArrayList<PackageInfo>();

        mAdapter.removeItems();

        for (PackageInfo pi : packages) {
            if ((pi.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0
                    && !AppList.isDefaultAppPackage(pi.packageName)) {
                filteredPackages.add(pi);
            }
        }

        if (filteredPackages.isEmpty()) {
            setPackageEmptyView(true);
        } else {
            setPackageEmptyView(false);

            for (PackageInfo pi : filteredPackages) {
                String packageName = pi.packageName;
                mAdapter.addItem(packageName);
            }
        }
    }


    private void registerToMyApp(String packageName) {
        PackageInfo info = AppPackageItem.getPackageInfo(mContext, packageName);

        if (info == null) {
            Toast.makeText(mContext, R.string.package_info_failed, Toast.LENGTH_SHORT).show();
            return;
        }

        boolean result = LauncherItemManager.getInstance(mContext).isAppInstalled(info.packageName);

        if(result) {
//            LauncherItemManager.getInstance(getApplicationContext()).moveUserItemToEnd(
//                    new UserLauncherItem(this, info.packageName));
            Toast.makeText(mContext, R.string.package_already_registered, Toast.LENGTH_SHORT).show();
        } else {
            boolean addResult = LauncherItemManager.getInstance(mContext.getApplicationContext()).addUserItem(
                    new UserLauncherItem(mContext, info.packageName));
            if (addResult) Toast.makeText(mContext, R.string.package_add_complete, Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteApp(String packageName) {
        Uri uri = Uri.fromParts("package", packageName, null);
        Intent i = new Intent(Intent.ACTION_DELETE, uri);
        mContext.startActivity(i);
    }

    private void packageCheckDialogForDefaultApp(String packageName) {
        final String pkgName = packageName;

        Activity activity = getActivity();

        if (activity == null || activity.isFinishing()) return;

        final int appTitleId = AppPackageItem.getSystemAppNameWithPkgName(packageName);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        if (appTitleId != 0) {
            builder.setTitle(appTitleId);
        } else {
            final String appTitle = AppPackageItem.getAppNameWithPkgName(activity, packageName);
            builder.setTitle(appTitle);
        }
        builder.setItems(R.array.system_app_package_items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int index) {
                switch(index) {
                    case 0 :
                        Intent intent = mContext.getPackageManager().getLaunchIntentForPackage(pkgName);
                        mContext.startActivity(intent);
                        break;
                    case 1:
                    case 2:
                        break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void packageCheckDialog(String packageName) {
        final String pkgName = packageName;
        Activity activity = getActivity();

        if (activity.isFinishing()) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(AppPackageItem.getAppNameWithPkgName(mContext, pkgName));
        builder.setItems(R.array.package_items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int index) {
                switch(index) {
                    case 0:
                        Intent intent = mContext.getPackageManager().getLaunchIntentForPackage(pkgName);
                        mContext.startActivity(intent);
                        break;
                    case 1:
                        registerToMyApp(pkgName);
                        break;
                    case 2:
                        deleteApp(pkgName);
                        break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setPageView (){
        if(mTotalPage == 0){
            pagination_layout.setVisibility(View.GONE);
        }else if(mTotalPage == 1) {
            mPrevBtn.setVisibility(View.INVISIBLE);
            mNextBtn.setVisibility(View.INVISIBLE);
        }else {
            mPrevBtn.setVisibility(View.VISIBLE);
            mNextBtn.setVisibility(View.VISIBLE);
        }
    }

}
