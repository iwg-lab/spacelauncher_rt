package com.inno.SpaceLauncher.config;


import com.inno.SpaceLauncher.R;

public class AppList {

    public static final String PRIME_DIC = "com.doosandonga.app.dictionary.primekorkor";

    public static final String SETTING = "com.onyx";

    public static final String GUIDE = "com.inno.innoguide";

    public static final String MUSIC = "com.iwg.music";

    public static final String[] DEFAULT_APP_PACKAGE_LIST = {
            GUIDE, PRIME_DIC, MUSIC, SETTING
    };

    public static boolean isDefaultAppPackage(String appPackageName){
        boolean isDefaultPAppPackage = false;
        for(String defaultAppPackageName : DEFAULT_APP_PACKAGE_LIST){
            if(defaultAppPackageName.equals(appPackageName)){
                isDefaultPAppPackage = true;
                break;
            }
        }
        return isDefaultPAppPackage;
    }

    public static final int[] DEFAULT_APP_PACKAGE_LIST_IMAGE_ID = {

            R.drawable.main_app_howtouse,
            R.drawable.main_app_dic,
            R.drawable.main_app_music,
            R.drawable.main_app_setting,

    };

    public static int getAppIconId(String appPackageName) {
        int c=0;
        for (String defaultAppPackageName : DEFAULT_APP_PACKAGE_LIST) {
            if (defaultAppPackageName.equals(appPackageName)) {

                break;
            }
            c++;
        }
        return DEFAULT_APP_PACKAGE_LIST_IMAGE_ID[c];
    }

    public static  final int[] DEFAULT_APP_PACKAGE_LIST_TITLE_ID = {
            R.string.main_app_howtouse,
            R.string.main_app_dictionary,
            R.string.main_app_music,
            R.string.main_app_setting
    };

}
