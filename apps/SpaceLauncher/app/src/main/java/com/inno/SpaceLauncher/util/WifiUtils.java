package com.inno.SpaceLauncher.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;

import androidx.annotation.RequiresApi;

public class WifiUtils {

    private WifiUtils(){

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static boolean isWifiConnected(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
       /* if(cm.isDefaultNetworkActive()){
            return cm != null && cm.i
        }
        return false;*/

        if (cm != null) {
            NetworkInfo ni = cm.getActiveNetworkInfo();
            return ni != null && ni.isConnectedOrConnecting();
        }
        return false;
    }



    public static boolean isWifiEnabled(Context context) {
//        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//        return wifi != null && wifi.isWifiEnabled();
        return true;
    }

    public static void enableWifi(Context context, boolean enable) {
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifi != null) {
            wifi.setWifiEnabled(enable);
        }
    }
}
