package com.inno.SpaceLauncher.install;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.inno.SpaceLauncher.R;
import com.inno.SpaceLauncher.install.fragment.AppInstallFragment;
import com.inno.SpaceLauncher.install.fragment.AppManagementFragment;
import com.inno.SpaceLauncher.view.TabView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class UserAppActivity extends AppCompatActivity implements TabView.OnTabListener{

    private final List<File> mFileList = new ArrayList<File>();

    private AlertDialog mAlertDialog;
    private StorageMountReceiver mStorageReceiver;
    private PackageManager mPackageManager;
    private RecyclerView mRecyclerView;
    private UserAppAdapter mUserAppAdapter;

    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_app);
        TabView mTabView = (TabView) findViewById(R.id.tab_layout);
        mTabView.addTab(R.string.tab_install);
        mTabView.addTab(R.string.tab_management);
        mTabView.setOnTabListener(this);
        mTabView.selectTab(0);

        mTabView.setFocusable(true);

        ImageView backButton = (ImageView) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
/*
        mPackageManager = mContext.getPackageManager();

        //mRecyclerView = findViewById(R.id.apk_list);
        mUserAppAdapter = new UserAppAdapter(mContext,mFileList);
        //mRecyclerView.setAdapter(mCo);*/

    }

    @Override
    public void onTabSelected(int position) {
        Fragment fragment;
        switch (position) {
            default:
            case 0:
                fragment = new AppInstallFragment();
                break;
            case 1:
                fragment = new AppManagementFragment();
                break;
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.user_app_page_layout, fragment)
                .commit();
    }



    @Override
    public void onTabUnselected(int position) {

    }

    @Override
    public void onTabReselected(int position) {

    }


    class StorageMountReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_MEDIA_MOUNTED) || action.equals(Intent.ACTION_MEDIA_EJECT)) {
                //refreshFile();
            }
        }
    }
}
