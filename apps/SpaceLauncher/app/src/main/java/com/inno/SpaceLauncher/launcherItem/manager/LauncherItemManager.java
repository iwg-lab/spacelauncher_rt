package com.inno.SpaceLauncher.launcherItem.manager;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.inno.SpaceLauncher.launcherItem.AddLauncherItem;
import com.inno.SpaceLauncher.launcherItem.LauncherItem;
import com.inno.SpaceLauncher.launcherItem.UserLauncherItem;
import com.inno.SpaceLauncher.util.pref.PrefUtils;

import java.util.ArrayList;
import java.util.List;


public class LauncherItemManager {

    private static final String TAG = "LauncherItemManager";

    public static final int MAX_ITEM_COUNT = 15;

    public static final int ADD_ITEM_ICON_COUNT = 1;

    private static final String PREF_INSTALLED_ITEMS = "installed_items";

    private static LauncherItemManager sInstance;

    private Context mContext;

    private List<LauncherItem> mItems;

    private LauncherItem mItemToMove;

    private int mDefaultItemCount;

    private UserLauncherItem mLastUserItemAdded;

    private LauncherItemManagerListener mListener;


    /**
     * Manager instance 생성. 앱 실행 초기에 한번 실행 필요
     */
    public static void createInstance(Context context) {
        if (sInstance != null) {
            sInstance.resetItems();
        }
        if (sInstance == null) {
            sInstance = new LauncherItemManager(context);
        }
    }

    public static LauncherItemManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new LauncherItemManager(context);
        }
        return sInstance;
    }

    private LauncherItemManager(Context context) {
        // NOTE: receiver 에서도 호출하므로 application context 사용 필요
        mContext = context.getApplicationContext();
        // user item 복구
        restoreUserItems();
    }

    private void resetItems() {
        mDefaultItemCount = 0;
        restoreUserItems();
    }

    public void registerListener(LauncherItemManagerListener listener) {
        mListener = listener;
        if (mListener != null) {
            mListener.onItemChanged(null);
        }
    }

    public void unregisterListener() {
        mListener = null;
    }

    public void callOnItemChanged(){
        if (mListener != null) {
            mListener.onItemChanged(null);
        }
    }

    public List<LauncherItem> getItems() {
        return mItems;
    }

    public int getItemCount() {
        return mItems.size();
    }

    public int getUserItemCount() {
        int cnt = 0;
        for (LauncherItem item : mItems) {
            if (item instanceof UserLauncherItem) {
                cnt++;
            }
        }
        return cnt;
    }

    ///////////////////////////////////////////////////////////////////////////
    // LauncherItem
    ///////////////////////////////////////////////////////////////////////////
    //region LauncherItem
    public void addDefaultItem(LauncherItem item) {
        if (item == null) return;

        mItems.add(mDefaultItemCount++, item);

        // AddLauncherItem 추가
        appendAddLauncherItem();
    }

    /**
     * Register app
     */
    public boolean addUserItem(UserLauncherItem item) {
        if (item == null) return false;

        boolean alreadyExist = false;
        // try updating existing item
        int c = 0;
        for (LauncherItem launcherItem : mItems) {
            if (launcherItem instanceof UserLauncherItem) {
                UserLauncherItem i = (UserLauncherItem) launcherItem;
                if (item.getPackageName().equals(i.getPackageName())) {
                    // update item
                    mItems.set(c, item);
                    alreadyExist = true;
                    break;
                }
            }
            c++;
        }

        // add item
        if (!alreadyExist) {
            removeAddLauncherItem();
            if (getItemCount() == MAX_ITEM_COUNT - ADD_ITEM_ICON_COUNT ) {
                Log.w(TAG, "Max launcher item reached " + MAX_ITEM_COUNT);
                //Toast.makeText(mContext, R.string.max_icon_count_msg, Toast.LENGTH_SHORT).show();
                appendAddLauncherItem();
                return false;
            }
            mItems.add(item);
            appendAddLauncherItem();
        }

        mLastUserItemAdded = item;

        // save preference
        saveUserItems();
        return true;
    }



    ///////////////////////////////////////////////////////////////////////////
    // Preference
    ///////////////////////////////////////////////////////////////////////////
    //region Preference
    /**
     * user item preference 저장
     */
    private void saveUserItems() {
        int cnt = getUserItemCount();
        if (cnt == 0) {
            PrefUtils.with().save(PREF_INSTALLED_ITEMS, "");
        } else {
            String[] s = new String[cnt];
            int i = 0;
            for (LauncherItem item : mItems) {
                if (item instanceof UserLauncherItem) {
                    s[i++] = ((UserLauncherItem) item).getPackageName();
                }
            }
            PrefUtils.with().save(PREF_INSTALLED_ITEMS, s);
        }
        if (mListener != null) {
            // NOTE: item is not used and invalid for swapItems
            mListener.onItemChanged(null);
        }
    }

    /***
     * user item preference 가져 오기
     */
    private void restoreUserItems() {
        // NOTE: actual valid max user item count is less than MAX_ITEM_COUNT due to default item
        String[] s = PrefUtils.with().get(PREF_INSTALLED_ITEMS, MAX_ITEM_COUNT, "");
        mItems = new ArrayList<LauncherItem>();
        for (String packageName : s) {
            if (!TextUtils.isEmpty(packageName)) {
                mItems.add(new UserLauncherItem(mContext, packageName));
            }
        }
    }


    ///////////////////////////////////////////////////////////////////////////
    // 아이콘 이동
    ///////////////////////////////////////////////////////////////////////////
    //region 아이콘 이동
    /**
     * 앱 아아콘 이동 시작시 호출
     */
    public void enterItemMoveMode(LauncherItem item) {
        mItemToMove = item;
        removeAddLauncherItem();
        if (mListener != null) {
            mListener.onEnterItemMove(item);
            mListener.onItemChanged(item);
        }
    }

    /**
     * 앱 아아콘 이동 취소시 호출
     */
    public void exitItemMoveMode() {
        mItemToMove = null;
        appendAddLauncherItem();
        if (mListener != null) {
            mListener.onExitItemMove();
            mListener.onItemChanged(null);
        }
    }

    /**
     * 앱 아아콘 이동 완료시 호출
     */


    public void exitItemMoveMode(LauncherItem targetItem) {
        if (mItemToMove == null || targetItem == null) {
            return;
        }
        // targetItem 과 아이콘 swap
        // swapItems(mItemToMove, targetItem);
        // targetItem 앞에 아이콘 insert
        //moveItemBefore(mItemToMove, targetItem);
        exitItemMoveMode();
    }

    public boolean isItemMoveMode() {
        return mItemToMove != null;
    }
    /**
     * 아이콘 이동시 선택된(이동할) item
     */
    public LauncherItem getItemToMove() {
        return mItemToMove;
    }

    public boolean moveUserItemToEnd(UserLauncherItem item) {
        removeUserItem(item);
        return addUserItem(item);
    }

    /**
     * Remove installed app
     */
    public boolean removeUserItem(UserLauncherItem item) {
        if (item == null) return false;

        for (LauncherItem launcherItem : mItems) {
            if (launcherItem instanceof UserLauncherItem) {
                UserLauncherItem i = (UserLauncherItem) launcherItem;
                if (item.getPackageName().equals(i.getPackageName())) {
                    mItems.remove(launcherItem);
                    Log.d(TAG, "removeUserItem > " + item.getPackageName());

                    appendAddLauncherItem();

                    // save preference
                    saveUserItems();
                    return true;
                }
            }
        }
        return false;
    }


    public boolean removeLastUserItem() {
        if (mLastUserItemAdded == null) {
            return false;
        }
        boolean result = removeUserItem(mLastUserItemAdded);
        mLastUserItemAdded = null;
        return result;
    }

    private void removeAddLauncherItem() {
        for (LauncherItem item : mItems) {
            if (item instanceof AddLauncherItem) {
                mItems.remove(item);
            }
        }
    }


    /**
     * AddLauncherItem 제일 뒤로 옮김
     */
    private void appendAddLauncherItem() {
        if (isItemMoveMode()) return;
        removeAddLauncherItem();
        if (getItemCount() < MAX_ITEM_COUNT) {
            mItems.add(new AddLauncherItem(mContext));
        }
    }



    ///////////////////////////////////////////////////////////////////////////
    // UTIL
    ///////////////////////////////////////////////////////////////////////////
    //region UTIL
    public boolean isAppInstalled(String packageName) {
        if (TextUtils.isEmpty(packageName)) return false;
        for (LauncherItem item : mItems) {
            if (item instanceof UserLauncherItem) {
                UserLauncherItem userLauncherItem = (UserLauncherItem) item;
                if (packageName.equals(userLauncherItem.getPackageName())) {
                    return true;
                }
            }
        }
        return false;
    }
    //endregion

}
