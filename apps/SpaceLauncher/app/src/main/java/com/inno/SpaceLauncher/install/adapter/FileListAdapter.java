package com.inno.SpaceLauncher.install.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inno.SpaceLauncher.R;
import com.inno.SpaceLauncher.install.ApkUtils;

import java.io.File;
import java.util.List;

public class FileListAdapter extends BaseAdapter {
    private final Context mContext;
    private List<File> mFileList;

    private int mStartIndex;

    public FileListAdapter(Context context, List<File> list) {
        mFileList = list;
        mContext = context;
    }

    @Override
    public int getCount() {
        int mTotalRow = mFileList.size();

        int mShowPerPage = 7;
        if (mShowPerPage > mTotalRow - mStartIndex){
            return mTotalRow - mStartIndex;
        } else {
            return mShowPerPage;
        }
    }

    @Override
    public Object getItem(int i) {
        return mFileList.get(i+ mStartIndex);
    }

    @Override
    public long getItemId(int i) {
        return i+ mStartIndex;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        FileListAdapter.ViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_select_apk, viewGroup, false);

            holder = new ViewHolder();
            holder.appIcon = (ImageView) view.findViewById(R.id.app_icon);
            holder.appName = (TextView) view.findViewById(R.id.app_name);
            holder.fileName = (TextView) view.findViewById(R.id.file_name);
            holder.locationSd = (TextView) view.findViewById(R.id.sd_location);
            /*holder.installable = (TextView) view.findViewById(R.id.installable);*/

            view.setTag(holder);
        } else {
            holder = (FileListAdapter.ViewHolder) view.getTag();
        }
        File file = mFileList.get(position+ mStartIndex);

        String appName = ApkUtils.getAppNameFromApk(mContext, file.getPath());
        Drawable appIcon = ApkUtils.getAppIconFromApk(mContext, file.getPath());

        holder.appIcon.setBackground(appIcon);
        holder.appName.setText(String.format("%s ", appName != null ? appName : mContext.getString(R.string.unknown)));
        holder.fileName.setText(String.format("%s", file.getName()));
        //holder.locationSd.setText(file.getParent().startsWith(INTERNAL_SD) ?
        //        R.string.internal_sd : R.string.external_sd);
        /*holder.installable.setText(String.format("%s", getInstallable(file)));*/
        return view;
    }

    public void updateFileList(List<File> list) {
        mFileList = list;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        ImageView appIcon;
        TextView appName;
        TextView fileName;
        TextView locationSd;
        /*public TextView installable;*/
    }

    public void setIndex (int index){
        mStartIndex = index;
    }

}
