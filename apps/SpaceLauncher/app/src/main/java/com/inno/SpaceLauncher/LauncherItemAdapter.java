package com.inno.SpaceLauncher;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inno.SpaceLauncher.launcherItem.LauncherItem;
import com.inno.SpaceLauncher.launcherItem.manager.LauncherItemManager;

import java.util.List;

public class LauncherItemAdapter extends BaseAdapter {

    private final LauncherItemManager mItemManager;
    private Context mContext;
    private int mLayoutId;

    public LauncherItemAdapter(Context context, int layoutId) {
        mContext = context;
        mLayoutId = layoutId;
        mItemManager = LauncherItemManager.getInstance(context);
    }

    @Override
    public int getCount() {
        return Math.min(mItemManager.getItemCount(), LauncherItemManager.MAX_ITEM_COUNT);
    }

    @Override
    public Object getItem(int position) {
        List<LauncherItem> items = mItemManager.getItems();
        if (position >= items.size()) return null;
        if (position >= LauncherItemManager.MAX_ITEM_COUNT) return null;
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(mLayoutId,null);
            viewHolder = new ViewHolder();
            viewHolder.mImageView = convertView.findViewById(R.id.sub_app_image);
            viewHolder.mTextView = convertView.findViewById(R.id.sub_app_text);
            convertView.setTag(viewHolder);
        } else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        LauncherItem launcherItem = (LauncherItem) getItem(position);
        if(launcherItem == null) return convertView;

        convertView.setBackground(null);

        int imageId = launcherItem.getImageId();
        if(imageId != 0 ){
            viewHolder.mImageView.setImageResource(imageId);
        }else {
            Drawable drawable = launcherItem.getImageDrawable();
            if(mItemManager.isItemMoveMode()){
                if(mItemManager.getItemToMove() == launcherItem){
                    convertView.setBackgroundResource(R.drawable.app_icon_holder_big);
                    viewHolder.mImageView.setImageDrawable(drawable);
                }else {
                    viewHolder.mImageView.setImageResource(R.drawable.app_icon_holder);
                }
            }else{
                viewHolder.mImageView.setImageDrawable(drawable);
            }
        }

        int titleId = launcherItem.getTitleId();
        if(titleId != 0){
            viewHolder.mTextView.setText(titleId);
        }else {
            viewHolder.mTextView.setText(launcherItem.getTitle());
        }

        return convertView;
    }

    private class ViewHolder {
        ImageView mImageView;
        TextView mTextView;
    }
}
