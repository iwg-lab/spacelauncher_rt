package com.inno.SpaceLauncher.launcherItem.manager;

import com.inno.SpaceLauncher.launcherItem.LauncherItem;

public interface LauncherItemManagerListener {

        void onItemChanged(LauncherItem item);

        void onEnterItemMove(LauncherItem item);

        void onExitItemMove();
}
