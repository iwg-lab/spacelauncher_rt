package com.inno.SpaceLauncher.install;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.inno.SpaceLauncher.R;
import com.inno.SpaceLauncher.config.AppList;
import com.inno.SpaceLauncher.launcherItem.UserLauncherItem;
import com.inno.SpaceLauncher.launcherItem.manager.LauncherItemManager;

import java.util.ArrayList;
import java.util.List;

public class PackageManagement extends Activity {

    private static final String TAG = "PackageManagement";

    private PackageManager mPackageManager;
    private LayoutInflater mInflater;
    private ListView mListView;
    private ListViewAdapter mAdapter;
    private Context mContext;

    private final ContentObserver mItemChangeObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            // reload items
            getInstalledPackages();
            mAdapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_popup);

        TextView title = (TextView) findViewById(R.id.label);
        title.setText(R.string.package_setting);

        TextView empty = (TextView) findViewById(R.id.package_empty);
        empty.setText(Html.fromHtml(getString(R.string.package_empty)));

        mContext = getApplicationContext();
        mPackageManager = getPackageManager();
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mAdapter = new ListViewAdapter();
        mListView = (ListView) findViewById(R.id.package_list);
        mListView.setAdapter(mAdapter);
        mListView.setEmptyView(empty);

        LinearLayout backButton = (LinearLayout) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.container).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getInstalledPackages();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getInstalledPackages();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        getContentResolver().unregisterContentObserver(mItemChangeObserver);

        super.onDestroy();
    }

    public void getInstalledPackages() {
        List<PackageInfo> packages = mPackageManager.getInstalledPackages(PackageManager.GET_META_DATA);
        List<PackageInfo> filteredPackages = new ArrayList<PackageInfo>();

        mAdapter.removeItems();

        TextView textView = (TextView) findViewById(R.id.package_empty);

        for (PackageInfo pi : packages) {
            if ((pi.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                filteredPackages.add(pi);
            }
        }

        if (filteredPackages.isEmpty()) {
            textView.setText(R.string.package_empty);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);

            for (PackageInfo pi : filteredPackages) {
                String packageName = pi.packageName;
                mAdapter.addItem(packageName);
            }
        }
    }

    private Drawable getIcon(String packageName) {
        try {
            return getPackageManager().getApplicationIcon(packageName);
        } catch (Exception ex) {
            return null;
        }
    }

    private String getAppName(String packageName) {
        try {
            return (String) getPackageManager()
                    .getApplicationLabel(getPackageManager()
                            .getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES));
        } catch(Exception ex){
            return "";
        }
    }

    private PackageInfo getPackageInfo(String packageName) {
        PackageManager packageManager = this.getPackageManager();
        try {
            return packageManager.getPackageInfo(packageName, 0);
        } catch (Exception ex) {
            return null;
        }
    }

    private void registerToMyApp(String packageName) {
        PackageInfo info = getPackageInfo(packageName);

        if (info == null) {
            Toast.makeText(mContext, R.string.package_info_failed, Toast.LENGTH_SHORT).show();
            return;
        }

        boolean result = LauncherItemManager.getInstance(this).isAppInstalled(info.packageName);

        if(result) {
//            LauncherItemManager.getInstance(getApplicationContext()).moveUserItemToEnd(
//                    new UserLauncherItem(this, info.packageName));
            Toast.makeText(mContext, R.string.package_already_registered, Toast.LENGTH_SHORT).show();
        } else {
            boolean addResult = LauncherItemManager.getInstance(getApplicationContext()).addUserItem(
                    new UserLauncherItem(this, info.packageName));
            if (addResult) Toast.makeText(mContext, R.string.package_add_complete, Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteApp(String packageName) {
        Uri uri = Uri.fromParts("package", packageName, null);
        Intent i = new Intent(Intent.ACTION_DELETE, uri);
        startActivity(i);
    }

    private int getSystemAppName(String packageName) {
        int appNameId = 0;

        /*if (AppList.KEPUB_CREMA.equals(packageName))
            appNameId = R.string.launcher_app_viewer;*/
        // TODO: 2021-01-04 Java6의 경우 string switch문 안됨. 다른 system 앱 이름 필요시 if문 추가하여 사용.

        return appNameId;
    }

    private void packageCheckDialogForDefaultApp(String packageName) {
        final String pkgName = packageName;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getSystemAppName(packageName));
        builder.setItems(R.array.system_app_package_items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int index) {
                switch(index) {
                    case 0 :
                        Intent intent = getPackageManager().getLaunchIntentForPackage(pkgName);
                        startActivity(intent);
                        break;
                    case 1:
                    case 2:
                        break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void packageCheckDialog(String packageName) {
        final String pkgName = packageName;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getAppName(pkgName));
        builder.setItems(R.array.package_items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int index) {
                switch(index) {
                    case 0:
                        Intent intent = getPackageManager().getLaunchIntentForPackage(pkgName);
                        startActivity(intent);
                        break;
                    case 1:
                        registerToMyApp(pkgName);
                        break;
                    case 2:
                        deleteApp(pkgName);
                        break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public class ListViewAdapter extends BaseAdapter {

        private ArrayList<AppPackageItem> listItem = new ArrayList<AppPackageItem>();

        @Override
        public int getCount() {
            return listItem.size();
        }

        @Override
        public Object getItem(int position) {
            return listItem.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;
            final Context context = parent.getContext();

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.app_list_item, parent, false);
            }

            ImageView appIcon = (ImageView) convertView.findViewById(R.id.icon);
            TextView appName = (TextView) convertView.findViewById(R.id.name);
            TextView packageName = (TextView) convertView.findViewById(R.id.package_name);
            TextView versionName = (TextView) convertView.findViewById(R.id.version);

            AppPackageItem item = listItem.get(position);
            appIcon.setImageDrawable(item.getAppIcon());
            appName.setText(item.getAppName());
            versionName.setText(item.getVersionName());
            packageName.setText(item.getPackageName());

            final String pkg = item.getPackageName();

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (AppList.isDefaultAppPackage(pkg)) {
                        packageCheckDialogForDefaultApp(pkg);
                    } else {
                        packageCheckDialog(pkg);
                    }
                }
            });

            return convertView;
        }

        public void addItem(String packageName) {
            AppPackageItem item = new AppPackageItem();

            item.setAppIcon(getIcon(packageName));
            item.setAppName(getAppName(packageName));
            item.setVersionName(getPackageInfo(packageName).versionName);
            item.setPackageName(packageName);

            listItem.add(item);
        }

        public void removeItems() {
            listItem.clear();
        }
    }

    public class AppPackageItem {
        private Drawable appIcon;
        private String appName;
        private String packageName;
        private String versionName;

        public Drawable getAppIcon() {
            return appIcon;
        }

        public void setAppIcon(Drawable appIcon) {
            this.appIcon = appIcon;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getVersionName() {
            return versionName;
        }

        public void setVersionName(String versionName) {
            this.versionName = versionName;
        }
    }
}