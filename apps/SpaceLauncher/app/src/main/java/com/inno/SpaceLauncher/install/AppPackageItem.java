package com.inno.SpaceLauncher.install;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import com.inno.SpaceLauncher.R;
import com.inno.SpaceLauncher.config.AppList;

public class AppPackageItem {
    private Drawable appIcon;
    private String appName;
    private String packageName;
    private String versionName;

    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        this.appIcon = appIcon;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public void setAllDataWithPkgName(Context context, String packageName){
        PackageInfo pi = getPackageInfo(context, packageName);

        setPackageName(packageName);
        setAppName(getAppNameWithPkgName(context, packageName));
        setAppIcon(getIconWithPkgName(context, packageName));
        setVersionName(pi==null ? "" : pi.versionName);
    }

    static public PackageInfo getPackageInfo(Context context, String packageName) {
        PackageManager packageManager = context.getPackageManager();
        try {
            return packageManager.getPackageInfo(packageName, 0);
        } catch (Exception ex) {
            return null;
        }
    }

    static public Drawable getIconWithPkgName(Context context, String packageName) {
        try {
            return context.getPackageManager().getApplicationIcon(packageName);
        } catch (Exception ex) {
            return null;
        }
    }

    static public String getAppNameWithPkgName(Context context, String packageName) {
        try {
            return (String) context.getPackageManager()
                    .getApplicationLabel(context.getPackageManager()
                            .getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES));
        } catch(Exception ex){
            return "";
        }
    }

    static public int getSystemAppNameWithPkgName(String packageName) {
        int appNameId = 0;

      /*  if (AppList.KEPUB_CREMA.equals(packageName))
            appNameId = R.string.launcher_app_viewer;*/
        // TODO: 2021-01-04 Java6의 경우 string switch문 안됨. 다른 system 앱 이름 필요시 if문 추가하여 사용.

        return appNameId;
    }
}
