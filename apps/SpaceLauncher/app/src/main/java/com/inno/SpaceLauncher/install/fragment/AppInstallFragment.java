package com.inno.SpaceLauncher.install.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.inno.SpaceLauncher.R;
import com.inno.SpaceLauncher.install.ApkUtils;
import com.inno.SpaceLauncher.install.PageUtils;
import com.inno.SpaceLauncher.install.adapter.FileListAdapter;
import com.inno.SpaceLauncher.launcherItem.UserLauncherItem;
import com.inno.SpaceLauncher.launcherItem.UserPendingLauncherItem;
import com.inno.SpaceLauncher.launcherItem.manager.LauncherItemManager;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_FIRST_USER;
import static android.app.Activity.RESULT_OK;

public class AppInstallFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    private static final String TAG = "AppInstallFragment";
    private static final boolean SHOW_INSTALLING_ICON = false;

    private static final String INTERNAL_SD = Environment.getExternalStorageDirectory().getPath();
    private static final String EXTERNAL_SD = "/mnt/external_sd/";
    private static final String SEND_ANYWHERE_DIR = "SendAnywhere";

    private ListView mListView;
    private final List<File> mFileList = new ArrayList<File>();
    private LayoutInflater mInflater;

    private AlertDialog mAlertDialog;
    private StorageMountReceiver mStorageReceiver;
    private PackageManager mPackageManager;
    private FileListAdapter mFileListAdapter;
    private PageUtils pageUtils;

    ImageView mPrevBtn, mNextBtn;
    TextView tv_page;
    LinearLayout pagination_layout;

    private int mCurrentPage = 1;
    private int mTotalPage = 0;
    private int mShowPerPage = 7;
    private int mTotalRow = 0;
    private int mStartIndex = 0;

    private int mLastVisiblePosition;
    private int mScrollY;

    private Context mContext;

    private static final String[] sFoldersToList = new String[]{
            INTERNAL_SD,
            INTERNAL_SD + "/Download",
            INTERNAL_SD + "/" + SEND_ANYWHERE_DIR,
            EXTERNAL_SD,
    };
    private final List<SDFileObserver> mFileObservers = new ArrayList<SDFileObserver>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (mContext != null) {
            mContext = null;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_install, container, false);

        mPackageManager = mContext.getPackageManager();
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mStorageReceiver = new StorageMountReceiver();

        mListView = (ListView) view.findViewById(R.id.apk_list);
        mListView.setVisibility(View.GONE);
        tv_page = (TextView)view.findViewById(R.id.text_page);
        pagination_layout = (LinearLayout)view.findViewById(R.id.pagination_layout);


        TextView emptyView = (TextView) view.findViewById(R.id.empty_view);
        emptyView.setText(Html.fromHtml(getString(R.string.apk_install_help)));
        mListView.setEmptyView(emptyView);

        pageUtils = new PageUtils();
        mFileListAdapter = new FileListAdapter(mContext, mFileList);

        mStartIndex = pageUtils.getIndexRowStart(mShowPerPage, mCurrentPage);
        mFileListAdapter.setIndex(mStartIndex);

        mListView.setAdapter(mFileListAdapter);
        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);

        TextView description = (TextView) view.findViewById(R.id.description);
        description.setText(R.string.selection_apk_description);

        for (String folder : sFoldersToList) {
            mFileObservers.add(new SDFileObserver(folder));
        }

        refreshFile();
        mTotalPage = pageUtils.getTotalPage(mTotalRow,mShowPerPage);

        tv_page.setText(getString(R.string.tv_page, mCurrentPage, mTotalPage));

        mPrevBtn = (ImageView) view.findViewById(R.id.prevBtn);
        mPrevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCurrentPage > 1) {
                    try{
                        mCurrentPage--;
                        int index = pageUtils.getIndexRowStart(mShowPerPage, mCurrentPage);
                        mFileListAdapter.setIndex(index);
                        mListView.setAdapter(mFileListAdapter);
                        tv_page.setText(getString(R.string.tv_page, mCurrentPage, mTotalPage));
                        mPrevBtn.setEnabled(true);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });

        mNextBtn = (ImageView)view.findViewById(R.id.nextBtn);
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCurrentPage < mTotalPage){
                    try {
                        mCurrentPage++;
                        int index = pageUtils.getIndexRowStart(mShowPerPage, mCurrentPage);
                        mFileListAdapter.setIndex(index);
                        mListView.setAdapter(mFileListAdapter);
                        tv_page.setText(getString(R.string.tv_page, mCurrentPage, mTotalPage));
                        mNextBtn.setEnabled(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });

        setPageView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        refreshFile();

        mListView.setSelectionFromTop(mLastVisiblePosition, mScrollY);

        for (AppInstallFragment.SDFileObserver sfo : mFileObservers) {
            sfo.startWatching();
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_EJECT);
        filter.addDataScheme("file");
        mContext.registerReceiver(mStorageReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();

        getListScrollInfo();

        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
        for (AppInstallFragment.SDFileObserver sfo : mFileObservers) {
            sfo.stopWatching();
        }
        mContext.unregisterReceiver(mStorageReceiver);
    }

    @Override
    public void onItemClick(final AdapterView<?> adapterView, View view, int position, long l) {
        ListView listView = (ListView) adapterView;
        final File file = (File) listView.getItemAtPosition(position);
        final String apkPath = file.getPath();
        final PackageInfo packageInfo = mPackageManager.getPackageArchiveInfo(apkPath,
                PackageManager.GET_ACTIVITIES);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        if (packageInfo != null && ApkUtils.isInstalledApk(mContext, packageInfo) == 0) {
            // 새로운 앱 설치
            installApk(apkPath);
            if (SHOW_INSTALLING_ICON) {
                packageInfo.applicationInfo.sourceDir = apkPath;
                packageInfo.applicationInfo.publicSourceDir = apkPath;
                String appName = (String) packageInfo.applicationInfo.loadLabel(mPackageManager);
                LauncherItemManager.getInstance(mContext).addUserItem(
                        new UserPendingLauncherItem(mContext, packageInfo.packageName, appName));
            }
        } else if (packageInfo != null && ApkUtils.isInstalledApk(mContext, packageInfo) == 1) {
            // 이미 설치 되어 있음
            boolean result = LauncherItemManager.getInstance(mContext).isAppInstalled(packageInfo.packageName);

            if (result) {
                // 이미 등록되어 있은 앱
                mAlertDialog = builder
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                LauncherItemManager.getInstance(mContext.getApplicationContext()).moveUserItemToEnd(
                                        new UserLauncherItem(mContext, packageInfo.packageName));
                                getActivity().finish();
                            }
                        })
                        .setMessage(getString(R.string.launcher_item_installed) + "\r\n" + getString(R.string.launcher_item_move))
                        .create();
            } else {
                // 설치는 되어 있지만 등록 되어 있지 않은 앱
                mAlertDialog = builder
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                LauncherItemManager.getInstance(mContext.getApplicationContext()).addUserItem(
                                        new UserLauncherItem(mContext, packageInfo.packageName));
                                getActivity().finish();
                            }
                        })
                        .setMessage(getString(R.string.launcher_item_installed) + "\r\n" + getString(R.string.launcher_item_add))
                        .create();
            }
            mAlertDialog.show();
        } else if (packageInfo != null && ApkUtils.isInstalledApk(mContext, packageInfo) == -1) {
            PackageInfo installedInfo = ApkUtils.getInstalledPackageInfo(mContext, packageInfo.packageName);
            LinearLayout msgLayout = (LinearLayout) mInflater.inflate(R.layout.file_version_info, null);

            ((TextView) msgLayout.findViewById(R.id.msg)).setText(R.string.launcher_item_installed_upper);
            ((TextView) msgLayout.findViewById(R.id.installed_info)).setText(installedInfo.versionName + " (" + installedInfo.versionCode + ")");
            ((TextView) msgLayout.findViewById(R.id.file_info)).setText(packageInfo.versionName + " (" + packageInfo.versionCode + ")");

            // 설치된 버전이 높은 경우
            mAlertDialog = builder
                    .setNegativeButton(android.R.string.ok, null)
                    .setView(msgLayout)
                    .create();
            mAlertDialog.show();

        } else {
            // 설치 불가 앱
            mAlertDialog = builder
                    .setNegativeButton(android.R.string.ok, null)
                    .setMessage(R.string.launcher_item_install_error).create();
            mAlertDialog.show();
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
        ListView listView = (ListView) adapterView;
        final File file = (File) listView.getItemAtPosition(position);
        String path = file.getPath();
        PackageInfo packageInfo = ApkUtils.getPackageInfoFromApk(mContext, path);
        String versionInfo = null;
        String name = null;
        Drawable icon = null;
        if (packageInfo != null) {
            name = ApkUtils.getAppNameFromApk(mContext, path);
            icon = ApkUtils.getAppIconFromApk(mContext, path);
            versionInfo = String.format("Version : %s\r\nVersionName : %s\r\n%s", packageInfo.versionCode, packageInfo.versionName, ApkUtils.getInstallable(mContext, file));
        } else {
            name = getString(R.string.unknown);
            icon = ApkUtils.getAppIconFromApk(mContext, path);
            versionInfo = String.format("Version : %s\r\nVersionName : %s\r\n%s", getString(R.string.unknown), getString(R.string.unknown), ApkUtils.getInstallable(mContext, file));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        mAlertDialog = builder.setPositiveButton(android.R.string.ok, null)
                .setTitle(name)
                .setIcon(icon)
                .setMessage(versionInfo)
                .create();
        mAlertDialog.show();
        return true;
    }

    /**
     * 리스트 스크롤 위치 기억을 위해
     */
    private void getListScrollInfo() {
        mLastVisiblePosition = mListView.getFirstVisiblePosition();
        View v = mListView.getChildAt(0);
        if (v != null)
            mScrollY = v.getTop();
        else
            mScrollY = 0;

    }

    /**
     * 앱 설치
     *
     * @param apkPath 설치할 파일 경로
     */
    public void installApk(String apkPath) {
        File apkFile = new File(apkPath);
        Uri apkUri = FileProvider.getUriForFile(mContext, mContext.getApplicationContext().getPackageName() + ".files", apkFile);
        Intent installApk = new Intent(Intent.ACTION_VIEW);
        installApk.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        installApk.setDataAndType(apkUri, "application/vnd.android.package-archive");
        installApk.putExtra(Intent.EXTRA_RETURN_RESULT, true);

        startActivityForResult(installApk, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult() requestCode >" + requestCode + " resultCode >" + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                // 정상 설치 완료
                getActivity().finish();
            } else if (resultCode == RESULT_FIRST_USER) {
                // 설치 오류
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                if (data != null) {
                    final String EXTRA_INSTALL_RESULT = "android.intent.extra.INSTALL_RESULT";
                    int installResult = data.getIntExtra(EXTRA_INSTALL_RESULT, 0);
//                    int installResult = data.getIntExtra(Intent.EXTRA_INSTALL_RESULT, 0);

                    final int INSTALL_FAILED_ALREADY_EXISTS = -1;
                    final int INSTALL_FAILED_INSUFFICIENT_STORAGE = -4;

                    switch (installResult) {
//                        case PackageManager.INSTALL_FAILED_ALREADY_EXISTS:
                        case INSTALL_FAILED_ALREADY_EXISTS:
                            builder.setMessage(R.string.launcher_item_installed);
                            break;
//                        case PackageManager.INSTALL_FAILED_INSUFFICIENT_STORAGE :
                        case INSTALL_FAILED_INSUFFICIENT_STORAGE :
                            builder.setMessage(R.string.launcher_item_install_freespace_error);
                            break;
                        default :
                            builder.setMessage(R.string.launcher_item_install_fail);
                            break;
                    }
                } else {
                    builder.setMessage(R.string.launcher_item_install_fail);
                }

                mAlertDialog = builder
                        .setNegativeButton(R.string.confirm, null)
                        .create();

                mAlertDialog.show();

                if (SHOW_INSTALLING_ICON) {
                    LauncherItemManager.getInstance(mContext).removeLastUserItem();
                }
            } else if (resultCode == RESULT_CANCELED) {
                // 설치 취소
                if (SHOW_INSTALLING_ICON) {
                    LauncherItemManager.getInstance(mContext.getApplicationContext()).removeLastUserItem();
                }
            }
        }
    }


    /**
     * apk 파일 리스트 생성
     *
     * @param path 리스트를 생성할 경로
     */
    private void getFileList(final String path) {
        if (TextUtils.isEmpty(path)) return;

        final File mCurrentPath = new File(path);
        if (mCurrentPath.isDirectory()) {
            // mFileList.clear();
            final File[] files = mCurrentPath.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    if (pathname.isDirectory()) {
                        return false;
                    } else {
                        String filePath = pathname.getPath();
                        return (filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length()))
                                .compareToIgnoreCase("apk") == 0;
                    }
                }
            });

            if (files != null) {
                mTotalRow = files.length;
            }

            if (files != null) {
                Collections.addAll(mFileList, files);
            }
            Collections.sort(mFileList, new Comparator<File>() {
                @Override
                public int compare(File lhs, File rhs) {
                    if (lhs.isDirectory() && rhs.isFile()) {
                        return -1;
                    } else if (lhs.isFile() && rhs.isDirectory()) {
                        return 1;
                    } else {
                        return lhs.getPath().compareToIgnoreCase(rhs.getPath());
                    }
                }
            });
        }
    }

    protected void refreshFile() {
        mFileList.clear();
        for (String folder : sFoldersToList) {
            getFileList(folder);
        }

        mFileListAdapter.updateFileList(mFileList);
        mFileListAdapter.setIndex(mStartIndex);
        mListView.setAdapter(mFileListAdapter);

    }

    public class StorageMountReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_MEDIA_MOUNTED) || action.equals(Intent.ACTION_MEDIA_EJECT)) {
                refreshFile();
            }
        }
    }

    private class SDFileObserver extends FileObserver {

        SDFileObserver(String path) {
            super(path);
        }

        @Override
        public void onEvent(int i, String path) {
            if (i == FileObserver.CLOSE_WRITE || i == FileObserver.DELETE) {
                if (path.toLowerCase().endsWith(".apk")) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            refreshFile();
                        }
                    });
                }
            }
        }
    }

    private void setPageView (){
        if(mTotalPage == 0){
            pagination_layout.setVisibility(View.GONE);
        }else if(mTotalPage == 1) {
            mPrevBtn.setVisibility(View.INVISIBLE);
            mNextBtn.setVisibility(View.INVISIBLE);
        }else {
            mPrevBtn.setVisibility(View.VISIBLE);
            mNextBtn.setVisibility(View.VISIBLE);
        }
    }

}
