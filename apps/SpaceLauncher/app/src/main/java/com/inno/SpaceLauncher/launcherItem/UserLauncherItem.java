package com.inno.SpaceLauncher.launcherItem;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class UserLauncherItem implements LauncherItem {

    private final Context mContext;

    private String mPackageName;

    //private final PackageUtils mPackageUtils;

    public UserLauncherItem(Context context, String packageName) {
        mContext = context;
        mPackageName = packageName;
        //mPackageUtils = new PackageUtils(context);
    }

    @Override
    public int getImageId() {
        return 0;
    }

    @Override
    public Drawable getImageDrawable() {
        return null;
    }

    @Override
    public int getTitleId() {
        return 0;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getPackageName() {
        return null;
    }

    @Override
    public void onClick() {

    }

    @Override
    public boolean onLongClick(Context context) {
        return false;
    }

    @Override
    public boolean needsWifiCheck() {
        return false;
    }

    @Override
    public boolean needsDeviceRegistrationCheck() {
        return false;
    }
}
