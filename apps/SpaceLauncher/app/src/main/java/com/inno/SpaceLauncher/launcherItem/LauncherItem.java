package com.inno.SpaceLauncher.launcherItem;

import android.content.Context;
import android.graphics.drawable.Drawable;

public interface LauncherItem {

    int getImageId();

    Drawable getImageDrawable();

    int getTitleId();

    String getTitle();

    String getPackageName();

    void onClick();

    boolean onLongClick(Context context);

    boolean needsWifiCheck();

    boolean needsDeviceRegistrationCheck();

}
