package com.inno.SpaceLauncher.install.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inno.SpaceLauncher.R;
import com.inno.SpaceLauncher.install.AppPackageItem;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter {
    private final ArrayList<AppPackageItem> listItem = new ArrayList<AppPackageItem>();
    private final Context mContext;
    private OnListItemClickListener mOnListItemClickListener;

    private int mStartIndex;

    public interface OnListItemClickListener {
        void onItemClick(AppPackageItem item);
    }

    public ListViewAdapter(Context context) {
        mContext = context;
    }

    public void setOnListItemClickListener(OnListItemClickListener mListener) {
        mOnListItemClickListener = mListener;
    }

    @Override
    public int getCount() {
        int mTotalRow = listItem.size();

        int mShowPerPage = 7;
        if (mShowPerPage > mTotalRow - mStartIndex){
            return mTotalRow - mStartIndex;
        } else {
            return mShowPerPage;
        }

    }

    @Override
    public Object getItem(int position) {
        return listItem.get(position+mStartIndex);
    }

    @Override
    public long getItemId(int position) {
        return position+mStartIndex;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //final int pos = position;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.app_list_item, parent, false);
        }

        ImageView appIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView appName = (TextView) convertView.findViewById(R.id.name);
        TextView packageName = (TextView) convertView.findViewById(R.id.package_name);
        TextView versionName = (TextView) convertView.findViewById(R.id.version);

        final AppPackageItem item = listItem.get(position+ mStartIndex);
        appIcon.setImageDrawable(item.getAppIcon());
        appName.setText(item.getAppName());
        versionName.setText(item.getVersionName());
        packageName.setText(item.getPackageName());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnListItemClickListener.onItemClick(item);
            }
        });

        return convertView;
    }

    public void addItem(String packageName) {
        AppPackageItem item = new AppPackageItem();
        item.setAllDataWithPkgName(mContext, packageName);
        listItem.add(item);
    }

    public void removeItems() {
        listItem.clear();
    }

    public void setIndex (int index){
        mStartIndex = index;
    }
}
