package com.inno.SpaceLauncher.util.pref;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.BaseAdapter;

import java.util.HashMap;
import java.util.Map;

/**
 * Wrapper/helper for Android's {@link SharedPreferences}
 */
public class PrefUtils extends BasePrefs {

    private static volatile PrefUtils singleton;
    private static Context sAppContext;
    private static Map<String, PrefUtils> sPrefCache = new HashMap<String, PrefUtils>();

    public static void createInstance(Context context) {
        if (singleton == null) {
            synchronized (PrefUtils.class) {
                if (singleton == null) {
                    sAppContext = context.getApplicationContext();
                    singleton = new PrefUtils(sAppContext, null);
                }
            }
        }
    }

    public static PrefUtils with() {
        if (singleton == null) {
            throw new RuntimeException("Call PrefUtils.createInstance first");
        }
        return singleton;
    }


    public static PrefUtils with(String prefFileName) {
        PrefUtils prefUtils = sPrefCache.get(prefFileName);
        if (prefUtils == null) {
            if (sAppContext == null) {
                throw new IllegalStateException("first call createInstance");
            }
            prefUtils = new PrefUtils(sAppContext, prefFileName);
            sPrefCache.put(prefFileName, prefUtils);
        }
        return prefUtils;
    }

    PrefUtils(Context context, String name) {
        super(context, name);
    }

    public Context getContext() {
        return sAppContext;
    }
}
