package com.inno.SpaceLauncher.install;

public class PageUtils {

    int mTotalPage = 0;
    int mIndexRowStart = 0;


    public int getIndexRowStart(int mShowPerPage, int mCurrentPage) {
        return  mIndexRowStart = ((mShowPerPage * mCurrentPage) - mShowPerPage);
    }

    public int getTotalPage(int mTotalRow, int mShowPerPage){
        if(mTotalRow == 0){
            return mTotalPage = 0;
        }else if(mTotalRow == mShowPerPage){
            return  mTotalPage = 1;
        }else if ((mTotalRow % mShowPerPage) == 0){
            return  mTotalPage = (mTotalRow / mShowPerPage);
        } else {
            return  mTotalPage = (mTotalRow / mShowPerPage) + 1;
        }
    }


}
