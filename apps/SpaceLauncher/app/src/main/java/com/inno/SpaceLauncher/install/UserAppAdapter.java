package com.inno.SpaceLauncher.install;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inno.SpaceLauncher.R;

import java.io.File;
import java.util.List;

public class UserAppAdapter extends BaseAdapter {

    private final Context mContext;
    private List<File> mFileList;

    public UserAppAdapter(Context mContext, List<File> list) {
        this.mContext = mContext;
        mFileList = list;
    }


    @Override
    public int getCount() {
        return mFileList.size();
    }

    @Override
    public Object getItem(int position) {
        return mFileList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UserAppAdapter.ViewHolder holder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.app_list_item_2, parent, false);

        }

        File file = mFileList.get(position);

        String appName = ApkUtils.getAppNameFromApk(mContext, file.getPath());
        Drawable appIcon = ApkUtils.getAppIconFromApk(mContext, file.getPath());
        holder = new ViewHolder();
        holder.appIcon = (ImageView) convertView.findViewById(R.id.apk_icon);
        holder.appName.setText(String.format("%s ", appName != null ? appName : mContext.getString(R.string.unknown)));

        return convertView;
    }

    public void updateFileList(List<File> list) {
        mFileList = list;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        ImageView appIcon;
        TextView appName;
        TextView fileName;
        TextView locationSd;
    }
}
