package com.inno.SpaceLauncher.install;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import androidx.core.content.res.ResourcesCompat;

import com.inno.SpaceLauncher.R;

import java.io.File;
import java.util.List;

public class ApkUtils {

    /**
     * App 이름 가져오기
     *
     * @param path
     * @return
     */
    static public String getAppNameFromApk(Context context, final String path) {
        PackageManager mPackageManager = context.getPackageManager();
        PackageInfo pi = mPackageManager.getPackageArchiveInfo(path, 0);

        if (pi != null) {
            pi.applicationInfo.sourceDir = path;
            pi.applicationInfo.publicSourceDir = path;
            return (String) pi.applicationInfo.loadLabel(mPackageManager);
        } else {
            return null;
        }
    }

    /**
     * apk파일에서 packageinfo 가져오기
     *
     * @param path
     * @return
     */
    static public PackageInfo getPackageInfoFromApk(Context context, final String path) {
        PackageManager mPackageManager = context.getPackageManager();
        return mPackageManager.getPackageArchiveInfo(path, 0);
    }

    /**
     * 설치된 apk의 packageinfo
     *
     * @param packageName
     * @return
     */
    static public PackageInfo getInstalledPackageInfo(Context context, String packageName) {
        PackageManager mPackageManager = context.getPackageManager();
        List<PackageInfo> packages = mPackageManager.getInstalledPackages(PackageManager.GET_META_DATA);
        for (PackageInfo pi : packages) {
            if (pi.packageName.equals(packageName)) {
                return pi;
            }
        }
        return null;
    }

    /**
     * App 아이콘 가져오기
     *
     * @param path
     * @return
     */
    static public Drawable getAppIconFromApk(Context context, final String path) {
        PackageManager mPackageManager = context.getPackageManager();
        PackageInfo pi = mPackageManager.getPackageArchiveInfo(path, 0);

        if (pi != null) {
            pi.applicationInfo.sourceDir = path;
            pi.applicationInfo.publicSourceDir = path;
            return pi.applicationInfo.loadIcon(mPackageManager);
        } else {
            return context.getResources().getDrawable(R.drawable.ic_launcher);
        }
    }

    /**
     * 앱 설치 여부
     *
     * @param packageInfo
     * @return
     */
    static public int isInstalledApk(Context context, PackageInfo packageInfo) {
        PackageManager mPackageManager = context.getPackageManager();
        List<PackageInfo> packages = mPackageManager.getInstalledPackages(PackageManager.GET_META_DATA);
        for (PackageInfo pi : packages) {
            if (packageInfo.packageName.equals(pi.packageName)) {
                //if(packageInfo.getLongVersionCode() == pi.getLongVersionCode())
                if (packageInfo.versionCode == pi.versionCode) {
                    return 1;
                } else if (packageInfo.versionCode < pi.versionCode) {
                    return -1;
                }
            }
        }
        return 0;
    }

    static public String getInstallable(Context context, File file) {
        PackageManager mPackageManager = context.getPackageManager();
        PackageInfo packageInfo = mPackageManager.getPackageArchiveInfo(file.getPath(), 0); //설치할 파일 정보

        if (packageInfo == null) {
            return context.getString(R.string.impossible_install);
        }

        List<PackageInfo> packages = mPackageManager.getInstalledPackages(PackageManager.GET_META_DATA);
        for (PackageInfo pi : packages) {
            if (packageInfo.packageName.equals(pi.packageName)) {
                if (packageInfo.versionCode == pi.versionCode) {
                    return context.getString(R.string.installed);
                } else if (packageInfo.versionCode < pi.versionCode) {
                    return context.getString(R.string.upper_installed);
                }
            }
        }
        return context.getString(R.string.possible_install);
    }
}
