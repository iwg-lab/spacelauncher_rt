package com.inno.SpaceLauncher;

import static android.content.ContentValues.TAG;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.inno.SpaceLauncher.config.AppList;
import com.inno.SpaceLauncher.launcherItem.BuiltInLauncherItem;
import com.inno.SpaceLauncher.launcherItem.manager.LauncherItemManager;
import com.inno.SpaceLauncher.launcherItem.manager.LauncherItemManagerListener;
import com.inno.SpaceLauncher.launcherItem.LauncherItem;
import com.inno.innolib.InnoUtils;

import com.inno.SpaceLauncher.install.PackageManagerReceiver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LauncherActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, LauncherItemManagerListener {

    private static final String TAG = "LauncherActivity";

    private GridView mAppGridView;
    private LauncherItemAdapter mAdapter;

    private LauncherItemManager mItemManager;

    private PackageManagerReceiver mPackageManagerReceiver;

    private static LauncherItem mLauncherItemClickPending;

    Bitmap image1;
    private static final int GALLERY_REQUEST_CODE = 2; //CropImageActivity
    ImageView logo; //The ImageView for Name
    private Uri mImageUri = null; //imageview uri

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

//        mPackageManagerReceiver = new PackageManagerReceiver();
//        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_PACKAGE_ADDED);
//        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
//        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
//        intentFilter.addDataScheme("package");
//        registerReceiver(mPackageManagerReceiver, intentFilter);

        initLauncherItems();
        initView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Log.d(TAG, "requestCode:" + requestCode + ", resultCode:" + resultCode+ ", data:"+ data);

// IGNORE FOR NOW DONT DELETE. ONLY USED FOR PICKING IMAGES FOR WIDGET
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK){
            mImageUri = data.getData();

            logo.setImageURI(mImageUri);
            }
        }

    private void initView(){
        mAdapter  = new LauncherItemAdapter(getApplicationContext(), R.layout.grid_app_item);
        mAppGridView = findViewById(R.id.grid_view_sub_app);
        mAppGridView.setAdapter(mAdapter);
        mAppGridView.setOnItemClickListener(this);
        mAppGridView.setOnItemLongClickListener(this);


        // ImageView for Name
        logo = (ImageView) findViewById(R.id.image_view_logo);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startAlert();

                /*Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST_CODE);

                 */ // When selecting picture DONT DELETE YET
            }
        });

    }

    private void startAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        final EditText edittext = new EditText(getApplicationContext());
        edittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edittext.setSingleLine();
        alert.setMessage("Please Enter Your Name:");
        TextView title = new TextView(this);
        title.setText("Widget Settings");
        title.setGravity(Gravity.CENTER);
        title.setTextSize(30);
        title.setTextColor(Color.BLACK);
        title.setPadding(10,10,10,10);
        title.setTypeface(Typeface.DEFAULT_BOLD);
        alert.setCustomTitle(title);
        // alert.setTitle("Widget Settings");
        alert.setView(edittext);

        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (edittext.getText().toString().isEmpty()) {
                    Toast.makeText(LauncherActivity.this, "Enter your name", Toast.LENGTH_LONG).show();

                } else {
                    ImageView imageView1 = findViewById(R.id.image_view_logo);
                    imageView1.setImageBitmap(TextToBitmap(edittext.getText().toString() + "'s " + "eBook"));
                    //storeImage(image1);
                    Toast.makeText(LauncherActivity.this, "Successfully Saved", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }
    private Bitmap TextToBitmap(String text) {
        Typeface tf = Typeface.createFromAsset(getAssets(), "BigSnow.ttf");
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(72);
        paint.setColor(Color.BLACK);
        //paint.setTypeface(Typeface.create(Typeface.MONOSPACE,Typeface.BOLD_ITALIC));
        paint.setTypeface(tf);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent();
        int width = (int) (paint.measureText(text) + 0.5f);
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.drawText(text,0,baseline,paint);
        image1 = image;
        return image;

    }
    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();

        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    /** Create a File for saving an image or video */
    private File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        /*File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");

         */
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/");



        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="MI_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private void initLauncherItems(){
        LauncherItemManager.createInstance(this);
        mItemManager = LauncherItemManager.getInstance(this);

        mItemManager.addDefaultItem(new BuiltInLauncherItem(this,R.drawable.main_app_howtouse,
                R.string.main_app_howtouse, AppList.GUIDE,false,false));
        mItemManager.addDefaultItem(new BuiltInLauncherItem(this,R.drawable.main_app_dic,
                R.string.main_app_dictionary, AppList.PRIME_DIC,false,false));
        mItemManager.addDefaultItem(new BuiltInLauncherItem(this, R.drawable.main_app_music,
                R.string.main_app_music, AppList.MUSIC, false, false));
        mItemManager.addDefaultItem(new BuiltInLauncherItem(this, R.drawable.main_app_setting,
                R.string.main_app_setting, AppList.SETTING, false, false));
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mLauncherItemClickPending = null;

        LauncherItem item = (LauncherItem) mAdapter.getItem(position);
        item.onClick();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }

    @Override
    public void onItemChanged(LauncherItem item) {

    }

    @Override
    public void onEnterItemMove(LauncherItem item) {

    }

    @Override
    public void onExitItemMove() {

    }

    @Override
    protected void onStart() {
        super.onStart();
        InnoUtils.refreshEpdManual(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mItemManager.registerListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //mItemManager.unregisterListener();
    }

}

