package com.inno.SpaceLauncher.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inno.SpaceLauncher.R;

public class TabView extends LinearLayout implements View.OnClickListener {
    private static String TAG = "TabView";
    private static final int TEXT_SIZE = 16;
    private Context mContext;
    private int mSelectedPosition = -1;

    private int mTextSize;

    public TabView(Context context) {
        super(context);
        mContext = context;
    }

    public TabView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mTextSize = context.obtainStyledAttributes(attrs, R.styleable.TabView)
                .getDimensionPixelSize(R.styleable.TabView_TextSize, TEXT_SIZE);
    }

    public TabView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
    }

    public interface OnTabListener {
        void onTabSelected(int position);

        void onTabUnselected(int position);

        void onTabReselected(int position);
    }

    private OnTabListener mOnTabListener;

    @Override
    public void onClick(View view) {
        int index = indexOfChild(view);
        //Log.d(TAG, "tab index >> " + index);
        if (index >= 0) {
            selectTab(index);
        }
    }

    public void setOnTabListener(OnTabListener onTabListener) {
        mOnTabListener = onTabListener;
    }

    public void selectTab(int position) {
        if (mSelectedPosition != position) {
            View view = setSelected(position, true);
            view = setSelected(mSelectedPosition, false);
            mSelectedPosition = position;

        } else {
            if (mOnTabListener != null) {
                mOnTabListener.onTabReselected(mSelectedPosition);
            }
        }
    }

    public void selectNextTab() {
        int childCount = getChildCount();
        if (mSelectedPosition >= childCount - 1) {
            return;
        }
        selectTab(mSelectedPosition + 1);
    }

    public void selectPreviousTab() {
        if (mSelectedPosition == 0) {
            return;
        }
        selectTab(mSelectedPosition - 1);
    }

    public void addTab(int titleId) {
        addTab(mContext.getString(titleId));
    }

    public void addTab(String title) {
        //Log.d(TAG, "tab addTab >> " + mTextSize);
        RelativeLayout tab = (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.tab_button, this, false);
        TextView text = ((TextView) tab.findViewById(R.id.tab_btn_text));
        text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
        text.setText(title);
        tab.setTag(title);
        tab.setOnClickListener(this);
        addView(tab);
    }

    public void addTab(String title, String tag) {
        //Log.d(TAG, "tab addTab >> " + mTextSize);
        RelativeLayout tab = (RelativeLayout) LayoutInflater.from(mContext).inflate(R.layout.tab_button, this, false);
        TextView text = ((TextView) tab.findViewById(R.id.tab_btn_text));
        text.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);
        text.setText(title);
        tab.setTag(tag);
        tab.setOnClickListener(this);
        addView(tab);
    }

    private View setSelected(int position, boolean selected) {
        View childAt = getChildAt(position);
        if (childAt != null) {
            if (selected) {
                mOnTabListener.onTabSelected(position);
            }
            childAt.setSelected(selected);
        }
        return childAt;
    }
}
