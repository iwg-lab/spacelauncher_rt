package com.inno.SpaceLauncher.util.pref;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

/**
 * Wrapper/helper for Android's {@link SharedPreferences}
 */
public class BasePrefs {

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Static Fields
    // ===========================================================

    // ===========================================================
    // Static Initializers
    // ===========================================================

    // ===========================================================
    // Static Methods
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    protected final SharedPreferences preferences;

    // ===========================================================
    // Initializers
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    public BasePrefs(Context context) {
        this(context.getApplicationContext(), null);
    }

    public BasePrefs(Context context, String name) {
        if (context == null) {
            throw new IllegalArgumentException("Context cannot be null.");
        }
        if (android.text.TextUtils.isEmpty(name)) {
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
        } else {
            preferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        }
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public SharedPreferences getPreferences() {
        return preferences;
    }

    public SharedPreferences.Editor edit() {
        return preferences.edit();
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    /** @see SharedPreferences.Editor#putBoolean(String, boolean) */
    public void save(String key, boolean value) {
        preferences.edit().putBoolean(key, value).apply();
    }

    /** @see SharedPreferences.Editor#putString(String, String) */
    public void save(String key, String value) {
        preferences.edit().putString(key, value).apply();
    }

    /** @see SharedPreferences.Editor#putInt(String, int) */
    public void save(String key, int value) {
        preferences.edit().putInt(key, value).apply();
    }

    /** @see SharedPreferences.Editor#putFloat(String, float) */
    public void save(String key, float value) {
        preferences.edit().putFloat(key, value).apply();
    }

    /** @see SharedPreferences.Editor#putLong(String, long) */
    public void save(String key, long value) {
        preferences.edit().putLong(key, value).apply();
    }

    /** @see SharedPreferences.Editor#putStringSet(String, Set) */
    public void save(String key, Set<String> value) {
        preferences.edit().putStringSet(key, value).apply();
    }

    public void save(String key, String... values) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (String s : values) {
            if (i++ != 0) sb.append("`");
            sb.append(s);
        }
        preferences.edit().putString(key, sb.toString()).apply();
    }

    /** @see SharedPreferences#getBoolean(String, boolean) */
    public boolean get(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }

    /** @see SharedPreferences#getString(String, String) */
    public String get(String key) {
        return preferences.getString(key, null);
    }

    /** @see SharedPreferences#getString(String, String) */
    public String get(String key, String defValue) {
        return preferences.getString(key, defValue);
    }

    public int getInteger(String key, String defValue) {
        String value = preferences.getString(key, defValue);
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return Integer.parseInt(defValue);
        }
    }

    /** @see SharedPreferences#getInt(String, int) */
    public int get(String key, int defValue) {
        return preferences.getInt(key, defValue);
    }

    /** @see SharedPreferences#getFloat(String, float) */
    public float get(String key, float defValue) {
        return preferences.getFloat(key, defValue);
    }

    /** @see SharedPreferences#getLong(String, long) */
    public long get(String key, long defValue) {
        return preferences.getLong(key, defValue);
    }

    /** @see SharedPreferences#getStringSet(String, Set) */
    public Set<String> get(String key, Set<String> defValue) {
        return preferences.getStringSet(key, defValue);
    }

    public String[] get(String key, int length, String defVal) {
        String[] split = new String[length];
        Arrays.fill(split, defVal);
        int i = 0;
        for (String s : preferences.getString(key, "").split("`")) {
            split[i++] = s;
            if (i >= length) break;
        }
        return split;
    }

    /** @see SharedPreferences#getAll() */
    public Map<String, ?> getAll() {
        return preferences.getAll();
    }

    /** @see SharedPreferences.Editor#remove(String) */
    public void remove(String key) {
        preferences.edit().remove(key).apply();
    }

    /** @see SharedPreferences#contains(String) */
    public boolean contains(String key) {
        return preferences.contains(key);
    }

    /**
     * Toggle a saved boolean value.
     *
     * @param key      The name of the preference to modify.
     * @param defValue Values to return if this preference does not exist.
     */
    public void toggle(String key, boolean defValue) {
        save(key, !get(key, defValue));
    }

    // ===========================================================
    // Inner and Anonymous Classes and/or Interfaces
    // ===========================================================

}