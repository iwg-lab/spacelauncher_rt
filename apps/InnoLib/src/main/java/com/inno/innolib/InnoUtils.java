package com.inno.innolib;

import android.content.Context;
import android.content.Intent;

import java.util.Objects;

public class InnoUtils {
    private static final String TAG = "InnoUtils";

    public static final String ACTION_EPD_REFRESH = "android.inno.refresh";

    public static final String ACTION_SOFTKEY_HIDE = "com.inno.softkey.HIDE";

    public static final String ACTION_SHOW_SYSTEM_APPS = "show_system_apps";

    public static final String ACTION_HIDE_SYSTEM_APPS = "hide_system_apps";

    public static void refreshEpdManual(Context context) {
        context.sendBroadcastAsUser(new Intent(ACTION_EPD_REFRESH), android.os.Process.myUserHandle());
    }

    //region HOME/SOFTKEY
    ///////////////////////////////////////////////////////////////////////////
    // HOME/SOFTKEY control
    ///////////////////////////////////////////////////////////////////////////

    public static void enableHomeKey(boolean enable) {
        SystemUtils.sysPropSet("iwg.homekey.disabled", enable ? "y" : "n");
    }

    public static boolean isHomeKeyEnabled() {
        return Objects.equals(SystemUtils.sysPropGet("iwg.homekey.disabled", "true"), "true");
    }

    public static void enableSoftKey(boolean enable) {
        SystemUtils.sysPropSet("iwg.softkey.disabled", enable ? "y" : "n");
    }

    public static boolean isSoftKeyEnabled() {
        return Objects.equals(SystemUtils.sysPropGet("iwg.softkey.disabled", "true"), "true");
    }

    public static void showStatusBarSystemApps(Context context, boolean show) {
        context.sendBroadcastAsUser(new Intent(show ? ACTION_SHOW_SYSTEM_APPS : ACTION_HIDE_SYSTEM_APPS), android.os.Process.myUserHandle());
    }
}
