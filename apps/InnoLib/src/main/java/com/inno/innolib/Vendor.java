package com.inno.innolib;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;

public class Vendor {
    public static class Name {
        public static final String YES24 = "keph_yes24";
        public static final String ALADIN = "keph_aladin";
        public static final String BANDI = "keph_bandi";
        public static final String SUPER = "crema-os-test";
        public static final String UNREGISTERED = "unregistered";
    }

    public static boolean isRegistered(Context context) {
        String vendor = get(context);
        return !(Name.UNREGISTERED.equals(vendor) || TextUtils.isEmpty(vendor));
    }

    public static String get(Context context) {
        String vendor = Settings.System.getString(context.getContentResolver(), SystemSettings.DEVICE_VENDOR);

        return vendor == null ? Name.UNREGISTERED : vendor;
    }

    public static void set(Context context, String vendor) {
        Settings.System.putString(context.getContentResolver(), SystemSettings.DEVICE_VENDOR, vendor);
    }

}
