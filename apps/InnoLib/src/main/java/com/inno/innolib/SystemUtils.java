package com.inno.innolib;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

public class SystemUtils {
    private static final String TAG = "SystemUtils";

    //region SETUP WIZARD
    ///////////////////////////////////////////////////////////////////////////
    // SETUP WIZARD related
    ///////////////////////////////////////////////////////////////////////////
    public static boolean hasSetupWizardRun(Context context) {
        int done = Settings.System.getInt(context.getContentResolver(), Settings.System.SETUP_WIZARD_HAS_RUN, 0);
        return done == 1;
    }

    public static void setSetupWizardRun(Context context, boolean run) {
        Settings.System.putInt(context.getContentResolver(), Settings.System.SETUP_WIZARD_HAS_RUN, run ? 1 : 0);
    }
    //endregion

    public static boolean isAppRunning(Context context, String packageName){
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for(int i = 0; i < procInfos.size(); i++){
            if(procInfos.get(i).processName.equals(packageName)){
                return true;
            }
        }

        return false;
    }

    public static void sysPropSet(String key, String val) throws IllegalArgumentException {
        try {
            Class<?> SystemProperties = Class.forName("android.os.SystemProperties");

            //Parameters Types
            @SuppressWarnings("rawtypes")
            Class[] paramTypes = { String.class, String.class };
            Method set = SystemProperties.getMethod("set", paramTypes);

            //Parameters
            Object[] params = { key, val }; set.invoke(SystemProperties, params);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Log.e(TAG, "IllegalArgumentException e: " + e.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Exception e: " + e.toString());
        }
    }

    public static String sysPropGet(String key) {
        String ret = "";
        try {
            Class<?> SystemProperties = Class.forName("android.os.SystemProperties");
            //Parameters Types
            @SuppressWarnings("rawtypes")
            Class[] paramTypes = { String.class };
            Method get = SystemProperties.getMethod("get", paramTypes);
            //Parameters
            Object[] params = { key };
            ret = (String) get.invoke(SystemProperties, params);
        } catch (IllegalArgumentException e) {
            ret = "";
            e.printStackTrace();
            Log.e(TAG, "IllegalArgumentException e: "+e.toString());
        } catch (Exception e) {
            ret = "";
            e.printStackTrace();
            Log.e(TAG, "Exception e: "+e.toString());
        }

        return ret;
    }

    public static String sysPropGet(String key, String defValue) {
        String ret = sysPropGet(key);
        if (ret == null || ret.isEmpty()) ret = defValue;

        return ret;
    }
}
