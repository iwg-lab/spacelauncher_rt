package com.inno.innolib;

public class SystemSettings {
    public static final String DEVICE_VENDOR = "device_vendor";

    public static final String FRONT_LIGHT_ENABLED = "front_light_enabled";

    public static final String SCREEN_BRIGHTNESS = "screen_brightness";

    public static final String SCREEN_BRIGHTNESS_COLOR = "screen_brightness_color";

    public static final String EPD_REFRESH_PAGE = "epd_refresh_page";

    public static final String CLOSE_WIFI_DELAY = "close_wifi_delay";
}
